package ru.tsc.korosteleva.tm.command.system;

import ru.tsc.korosteleva.tm.api.model.ICommand;
import ru.tsc.korosteleva.tm.command.AbstractCommand;

import java.util.Collection;

public class ArgumentsCommand extends AbstractSystemCommand{

    public static final String NAME = "arguments";

    public static final String ARGUMENT = "-arg";

    public static final String DESCRIPTION = "Show argument list.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final AbstractCommand command : commands) {
            final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

}
