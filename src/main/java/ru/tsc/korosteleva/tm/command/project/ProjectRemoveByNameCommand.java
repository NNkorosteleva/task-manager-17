package ru.tsc.korosteleva.tm.command.project;

import ru.tsc.korosteleva.tm.model.Project;
import ru.tsc.korosteleva.tm.util.TerminalUtil;

public class ProjectRemoveByNameCommand extends AbstractProjectCommand {

    public static final String NAME = "project-remove-by-name";

    public static final String ARGUMENT = null;

    public static final String DESCRIPTION = "Remove project by name.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY NAME]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Project project = getProjectService().findOneByName(name);
        getProjectTaskService().removeProjectById(project.getId());
    }

}
