package ru.tsc.korosteleva.tm.service;

import ru.tsc.korosteleva.tm.api.repository.ITaskRepository;
import ru.tsc.korosteleva.tm.api.service.ITaskService;
import ru.tsc.korosteleva.tm.enumerated.Sort;
import ru.tsc.korosteleva.tm.enumerated.Status;
import ru.tsc.korosteleva.tm.exception.entity.TaskNotFoundException;
import ru.tsc.korosteleva.tm.exception.field.*;
import ru.tsc.korosteleva.tm.model.Task;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public Task add(final Task task) {
        if (task == null) throw new TaskNotFoundException();
        return taskRepository.add(task);
    }

    @Override
    public Task create(final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return taskRepository.create(name);
    }

    @Override
    public Task create(String name, String description, Date dateBegin, Date dateEnd) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        if (dateBegin == null) throw new DateIncorrectException();
        if (dateEnd == null) throw new DateIncorrectException();
        return taskRepository.create(name, description, dateBegin, dateEnd);
    }

    @Override
    public Task create(final String name, final String description) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return taskRepository.create(name, description);
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public List<Task> findAll(final Comparator comparator) {
        if (comparator == null) return findAll();
        return taskRepository.findAll(comparator);
    }

    @Override
    public List<Task> findAll(final Sort sort) {
        if (sort == null) return findAll();
        return taskRepository.findAll(sort);
    }

    @Override
    public List<Task> findAllByProjectId(final String projectId) {
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return taskRepository.findAllByProjectId(projectId);
    }

    @Override
    public Task findOneById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        Task task = taskRepository.findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        return task;
    }

    @Override
    public Task findOneByIndex(final Integer index) {
        if (index == null || index < 0 || index > taskRepository.getSize()) throw new IndexIncorrectException();
        Task task = taskRepository.findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        return task;
    }

    @Override
    public Task findOneByName(final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        Task task = taskRepository.findOneByName(name);
        if (task == null) throw new TaskNotFoundException();
        return task;
    }

    @Override
    public Task updateById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        Task task = taskRepository.updateById(id, name, description);
        if (task == null) throw new TaskNotFoundException();
        return task;
    }

    @Override
    public Task updateByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0 || index > taskRepository.getSize()) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        Task task = taskRepository.updateByIndex(index, name, description);
        if (task == null) throw new TaskNotFoundException();
        return task;
    }

    @Override
    public Task remove(final Task task) {
        if (task == null) throw new TaskNotFoundException();
        return taskRepository.remove(task);
    }

    @Override
    public Task removeById(final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        Task task = taskRepository.removeById(id);
        if (task == null) throw new TaskNotFoundException();
        return task;
    }

    @Override
    public Task removeByIndex(final Integer index) {
        if (index == null || index < 0 || index > taskRepository.getSize()) throw new IndexIncorrectException();
        Task task = taskRepository.removeByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        return task;
    }

    @Override
    public Task removeByName(final String name) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        Task task = taskRepository.removeByName(name);
        if (task == null) throw new TaskNotFoundException();
        return task;
    }

    @Override
    public Task changeTaskStatusById(final String id, final Status status) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        Task task = taskRepository.changeTaskStatusById(id, status);
        if (task == null) throw new TaskNotFoundException();
        return task;
    }

    @Override
    public Task changeTaskStatusByIndex(final Integer index, final Status status) {
        if (index == null || index < 0 || index > taskRepository.getSize()) throw new IndexIncorrectException();
        Task task = taskRepository.changeTaskStatusByIndex(index, status);
        if (task == null) throw new TaskNotFoundException();
        return task;
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

}