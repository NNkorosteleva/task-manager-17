package ru.tsc.korosteleva.tm.api.service;

public interface ILoggerService {

    void info(String massage);

    void debug(String massage);

    void command(String massage);

    void error(Exception e);

}
