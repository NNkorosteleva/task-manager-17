package ru.tsc.korosteleva.tm.api.service;

import ru.tsc.korosteleva.tm.enumerated.Sort;
import ru.tsc.korosteleva.tm.enumerated.Status;
import ru.tsc.korosteleva.tm.model.Task;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

public interface ITaskService {

    Task add(Task task);

    Task create(String name);

    Task create(String name, String description);

    Task create(String name, String description, Date dateBegin, Date dateEnd);

    List<Task> findAll();

    List<Task> findAll(Comparator comparator);

    List<Task> findAll(Sort sort);

    List<Task> findAllByProjectId(String projectId);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task findOneByName(String name);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

    Task remove(Task task);

    Task removeById(String id);

    Task removeByIndex(Integer index);

    Task removeByName(String name);

    Task changeTaskStatusById(String id, Status status);

    Task changeTaskStatusByIndex(Integer index, Status status);

    void clear();

}