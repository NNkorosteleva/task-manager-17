package ru.tsc.korosteleva.tm.api.repository;

public interface IProjectTaskRepository {

    void bindTaskToProject(String projectId, String taskId);

    void unbindTaskFromProject(String projectId, String taskId);

    void removeProjectById(String projectId);

}
