package ru.tsc.korosteleva.tm.api.controller;

public interface IProjectController {

    void createProject();

    void showProjectList();

    void clearProjects();

    void showProjectById();

    void showProjectByIndex();

    void updateProjectById();

    void updateProjectByIndex();

    void removeProjectById();

    void removeProjectByIndex();

    void removeProjectByName();

    void changeProjectStatusById();

    void changeProjectStatusByIndex();

    void startProjectById();

    void startProjectByIndex();

    void completeProjectById();

    void completeProjectByIndex();

}
